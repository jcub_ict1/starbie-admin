<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class branch extends Model
{
    protected $table  = 'branches';
    protected $primaryKey = 'bid';
    public $timestamp = false;

    public function Organizations(){
        return $this->belongsTo('Organizations::class');
       }  
}
