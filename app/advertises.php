<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class advertises extends Model
{
    protected $guarded = [];
    protected $table  = 'advertises';
    protected $primaryKey = 'ad_id';
    public $timestamps = false;
}
