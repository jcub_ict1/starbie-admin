<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stocks extends Model
{
    protected $table ='inventoryitem';
    protected $primaryKey = 'itemid';

    // timestamps
    public $timestamps =false;
}
    

