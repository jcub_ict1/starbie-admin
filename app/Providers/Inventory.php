<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $table ='inventory';
    protected $primaryKey = 'iid';

    // timestamps
    public $timestamps =false;
}
    

