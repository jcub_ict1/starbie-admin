<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;
use Auth;

use App\Customers;
use DB;
Use App\Organizations;


class TransactionsController extends Controller
{
    public function index(){
        $orgId = Auth::user()->organization_id;
        $org = Organizations::where('oid', $orgId)->first();


        // dd($org);
        //$data = DB::table('inventoryitem')->orderBy('itemid', 'DESC')->get();

        $data =DB::table('customer')
        ->join('transaction', 'customer.cid', '=', 'transaction.cid')
        // ->join('customer', 'transactionitem.itemid', '=', 'inventoryitem.itemid')
        // ->join('category', 'inventoryid.iid', '=', 'category.iid')
        // ->join('organizations', 'branches.oid', '=', 'organizations.oid')
        ->select('transaction.tid','transaction.date','transaction.totalprice'
           ,'transaction.paid','customer.cid','customer.fname','customer.mnames'
           ,'customer.lname')
        ->where('customer.oid', $orgId)
        ->get();
        //return view('stocks', compact('data'));   

 
        return view('transactions.index', compact('data'), array(
            'org' => $org
        ));   
    }

    //public function create(){
       //$transactions = new Transactions();
        //return view ('transactions.create', compact('transactions'));

    //}

    //public function store(){
        //$transactions = Transactions::create($this->validateData());
        //$this->storeLogo($transactions);
        //return redirect('transactions');
    //} 
    
    public function show($tid){
        $transactions =  Transactions::find($tid);
        return view('transactions.show')->with ('transactions',$transactions);

    }

     public function edit($tid)
    {
        $transactions = Transactions::find($tid);
        return view('transactions.edit')->with('transactions',$transactions);
        return redirect('/transactions')->with('success', 'Transaction Edited');

    }

    public function update(Request $request, $tid)
    {
        $this->validate($request,[
            'date'=>'required',
            'totalprice'=>'required'
        ]);
       
        // create customer
        $transaction = new Transactions();
        //$transaction->tid=$request->input('tid');
        $transaction->date=$request->input('date');
        $transaction->totalprice=$request->input('totalprice');
        $transaction->cid=$request->input('cid');
        $transaction->paid=$request->input('paid');  

                      
        $transaction->save();

        return redirect('/transactions')->with('success', 'Transaction Updated');
    }




    public function destroy(Transactions $transactions){
        $transactions->delete();
        return redirect('transactions');
    }

    // public function validateData(){
    //     return tap(request()->validate([
    //         'name'=>'required',
            
    //         'contactno1'=>'required',
    //         'contactno2'=>'',
    //         'headquarter'=>'required',
    //         'themecolor'=>'',
    //         'about'=>'required'
    //     ]), function(){
    //         if(request()->hasFile('logo')){
    //             request()->validate([
    //                 'logo'=>'file|image|max:5000',
    //             ]);
    //         }
    //     });
    // }
    // private function storeLogo($transactions){
    //     if(request()->has ('logo')){
    //         $transactions->update([
    //             'logo' => request()->logo->store('uploads','public'),
    //         ]);
    //     }
    // }
}