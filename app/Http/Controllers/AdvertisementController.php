<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\advertises;
use DB;

class AdvertisementController extends Controller
{
    public function index(){
        $advertises = advertises::all();
        return view ('advertises.index', compact('advertises'));
    }

    public function create(){
        $advertises = new advertises();
        return view ('advertises.create', compact('advertises'));

    }

    public function store(){
        $advertises = advertises::create($this->validateData());
        $this->storeLogo($advertises);
        return redirect('advertises');
    }

    public function show(advertises $advertises){
        return view ('advertises.show', compact('advertises'));
    }
    public function edit(advertises $advertises){
        return view ('advertises.edit', compact('advertises'));
    }

    public function update(advertises $advertises){
        $advertises->update($this->validateData());
        $this->storeLogo($advertises);
        return redirect ('advertises/'.$advertises->oid);
    }
    public function destroy(advertises $advertises){
        $advertises->delete();
        return redirect('advertises');
    }

    public function validateData(){
        return tap(request()->validate([
            'oname'=>'required',
            'ad_name'=>'required',
            'ad_message'=>'required',
        ]), function(){
            if(request()->hasFile('ad_image')){
                request()->validate([
                    'ad_image'=>'file|image|max:5000',
                ]);
            }
        });
    }
    private function storeLogo($advertises){
        if(request()->has ('ad_image')){
            $advertises->update([
                'ad_image' => request()->ad_image->store('uploads','public'),
            ]);
        }
    }
}
