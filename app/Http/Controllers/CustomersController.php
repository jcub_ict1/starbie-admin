<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use DB;

class CustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $customers=customer::all();
        // $customers = DB::select ('select * from customers');
        // return view ('customers.index')->with ('customers', $customers);

        
         $customers = customer::orderby('cid','asc')->get();
        
         return view('customers.index')->with ('customers', $customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
            return view('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $cid
     * @return \Illuminate\Http\Response
     */
    public function show($cid)
    {
        $customers =  customer::find($cid);
        return view('customers.show')->with ('customers',$customers);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $cid
     * @return \Illuminate\Http\Response
     */
    public function edit($cid)
    {
        $customers = customer::find($cid);
        return view('customers.edit')->with('customers',$customers);
        return redirect('/customers')->with('success', 'Customer Edited');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $cid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cid)
    {
        $this->validate($request,[
            'fname'=>'required',
            'lname'=>'required',
            'approved'=>'required'
        ]);
       
        // create customer
        $customer = Customer::find($cid);
        $customer->fname=$request->input('fname');
        $customer->mnames=$request->input('mnames');
        $customer->lname=$request->input('lname');
        $customer->dob=$request->input('dob');  
        $customer->gender=$request->input('gender');
        $customer->email=$request->input('email');
        $customer->oid=$request->input('oid');
        $customer->approved=$request->input('approved');
                      
        $customer->save();

        return redirect('/customers')->with('success', 'Customer updated');
    }


    public function approve(Request $request, $cid)
    {
       $customer = Customer::find($cid);
       $customer->approved= $request->input('approved',1) ;
       $customer->save();

    // Customer::where('cid',$request->get('cid'))->update(['approved'=>true]);
    return redirect('/customers')->with('success', 'Customer Approved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $cid
     * @return \Illuminate\Http\Response
     */
    public function destroy($cid)
    {
        $customer=customer::find($cid);
        $customer->delete();
        return redirect('/customers')->with ('success', 'Customer Deleted');
        
    }
}
