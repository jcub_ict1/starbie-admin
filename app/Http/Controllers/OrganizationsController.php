<?php

namespace App\Http\Controllers;

use App\Organizations;
use Illuminate\Http\Request;


class OrganizationsController extends Controller
{
    public function index(){
        $organizations = Organizations::all();
        return view ('organizations.index', compact('organizations'));
    }

    public function create(){
       $organizations = new Organizations();
        return view ('organizations.create', compact('organizations'));

    }

    public function store(){
        $organizations = Organizations::create($this->validateData());
        $this->storeLogo($organizations);
        return redirect('organizations');
    } 
    
    public function show(Organizations $organizations){
        return view ('organizations.show', compact('organizations'));
    }
    public function edit(Organizations $organizations){
        return view ('organizations.edit', compact('organizations'));
    }

    public function update(Organizations $organizations){
        $organizations->update($this->validateData());
        $this->storeLogo($organizations);
        return redirect ('organizations/'.$organizations->oid);
    }
    public function destroy(Organizations $organizations){
        $organizations->delete();
        return redirect('organizations');
    }

    public function validateData(){
        return tap(request()->validate([
            'name'=>'required',
            
            'contactno1'=>'required',
            'contactno2'=>'',
            'headquarter'=>'required',
            'themecolor'=>'',
            'about'=>'required'
        ]), function(){
            if(request()->hasFile('logo')){
                request()->validate([
                    'logo'=>'file|image|max:5000',
                ]);
            }
        });
    }
    private function storeLogo($organizations){
        if(request()->has ('logo')){
            $organizations->update([
                'logo' => request()->logo->store('uploads','public'),
            ]);
        }
    }
}