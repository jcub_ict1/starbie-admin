<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organizations extends Model
{
    protected $guarded =[];
    protected $primaryKey="oid";
    public $timestamps = false;
    protected $table = "organization";

   public function branches(){
    return $this->hasMany('branch::class');
   }  
}
