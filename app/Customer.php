<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //table name 
    protected $table  = 'customer';
    // Priamry key
    protected $primaryKey = 'cid';

    // timestamps
    public $timestamps =false;
}
