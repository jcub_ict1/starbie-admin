<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//route pages

use App\Http\Controllers\OrganizationsController;

Route::group(['middleware' => 'admin'], function () {
    Route::get('/dashboard', 'DashboardController@index');
//route admin
});

Route::group(['prefix' => 'super', 'middleware' => 'superadmin'], function () {
    Route::get('/dashboard', 'SuperAdminController@index')->name('superadmin-dashboard');
    Route::post('/create-user', 'SuperAdminController@createUser')->name('create-user');
    
    
//route admin
});




//route authentication
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 'PageController@index');


Route::group(['middleware' => 'auth'], function () {

//route posts
Route::resource('posts','PostsController');

Route::resource('advertises','AdvertisementController');

Route::resource('admin','adminController');

Route::resource('customers','CustomersController');
Route::PATCH('approve/{customers}','CustomersController@approve');

Route::resource('transactions','TransactionsController');

Route::resource('branch','branchcontroller');
Route::get('/branch/create', 'branchcontroller@showBranchForm');
Route::get('/branch/edit', 'branchcontroller@showBranchForm');

Route::get('organizations','OrganizationsController@index');
Route::get('organizations/create','OrganizationsController@create');
Route::POST('organizations','OrganizationsController@store');
Route::GET('organizations/{organizations}','OrganizationsController@show');
Route::GET('organizations/{organizations}/edit','OrganizationsController@edit');
Route::PATCH('organizations/{organizations}','OrganizationsController@update');
Route::DELETE('organizations/{organizations}','OrganizationsController@destroy');


Route::GET('/init-permission', 'PermissionController@initPermission');


Route::get('/inventory', 'InventoryItemController@index');
Route::get('downloadData/{type}', 'InventoryItemController@downloadData');
Route::post('importData', 'InventoryItemController@importData');

Route::get('/stocks', 'StocksController@index');


});

Route::get('/about', 'PageController@about');
Route::get('/services', 'PageController@services');