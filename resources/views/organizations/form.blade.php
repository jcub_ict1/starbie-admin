{{ csrf_field() }}

<div class="mb 5">
<div class = "form-group">
        <label for ="name">Name</label>
        <input type="text" name= "name" value="{{old('name') ?? $organizations->name}} " class ="form-control" placeholder="Organization Name">
         {{$errors->first('name')}}
    </div>
    <div class = "form-group">
        <label for ="contactno1">Mobile Number</label>
        <input type="text" name = "contactno1" value="{{old('contactno1')?? $organizations->contactno1}}" class ="form-control" placeholder="First Contact Number">
        {{$errors->first('contactno1')}}
    </div>
    <div class = "form-group">
        <label for ="contactno2">Telephone Number</label>
        <input type="text" name = "contactno2" value="{{old('contactno2')?? $organizations->contactno2}}" class ="form-control" placeholder="Second Contact Number">
        {{$errors->first('contactno2')}}
    </div>
    <div class = "form-group">
            <label for ="headquarter">Headquarter</label>
            <input type="text" name ="headquarter" value="{{old('headquarter')?? $organizations->headquarter}}" class ="form-control" placeholder="Headquarter">
            {{$errors->first('headquarter')}}
    </div>
    <div class = "form-group">
        <label for ="themecolor">Theme Color</label>
        <input type="text" name ="themecolor" value="{{old('themecolor')?? $organizations->themecolor}}" class ="form-control" placeholder="Theme Color For Browser">
        {{$errors->first('themecolor')}}
</div>
<div class = "form-group">
    <label for ="logo">Logo</label>
    <input type="file" name ="logo" value="{{old('logo')?? $organizations->logo}}" class ="form-control" placeholder="Please Upload Organization's Logo">
    {{$errors->first('logo')}}
</div>
    <div class = "form-group d-flex flex-column">
            <label for ="about">Description</label>
            <textarea name = "about" value="{{old('about')?? $organizations->about}}" cols="30" rows="10"class ="form-control" placeholder="Description about the Organization">
                   {{ $organizations->about}}
             </textarea>
            {{$errors->first('about')}}
    </div>
</div>
    

    @csrf