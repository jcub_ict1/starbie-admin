@extends('layouts.app')

@section('content')

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">
<hr>
 <div class="panel-heading">
 <h3>Transaction Details</h3>
 </div>
 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> Transaction ID</th>
 <th> Date</th>
 <th> Total Price </th>
 <th>Customer ID </th>
 <th> Paid Details </th>
 </thead>
<tbody>
<tr>
    <td>{{ $transactions->tid }}</td>            
    <td>{{ $transactions->date }}</td>
    <td> {{ $transactions->totalprice }}</td>
    <td> {{ $transactions->cid}}</td>
    @if($transactions->paid ==1)
         
           <td> <b>Paid<b></td>
         
         @else
           <td><b>Unpaid </b></td>
         
    @endif

    </tr>
    </tbody>
    <thead>

</table>
</div>
</section>
</div>
</div>

<div class="row">
<div class="col-md-6"> 
<a href="{{ route('transactions.index') }}" class="btn btn-info">Back to all transactions</a>
<a href="{{ route('transactions.edit', $transactions->tid) }}" class="btn btn-primary">Edit This transaction</a>
</div>


<div class="col-md-6 text-right">
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['transactions.destroy', $transactions->tid]
        ]) !!}
            {!! Form::submit('Delete this transaction', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
</div>


<hr>



@endsection