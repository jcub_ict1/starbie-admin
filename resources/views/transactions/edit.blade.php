@extends('layouts.app')

@section('content')

<div class="row">
<div class= "col-lg-12 col-sm-12">
<section class="panel panel-primary"> 
 <div class="panel-heading" > 
 <hr>
</h3><b>Edit the transaction</b></h3>
 </div>

<div class="panel-body"> 

{!! Form::model($transactions, [
    'method' => 'PATCH',
    'route' => ['transactions.update', $transactions->tid]
]) !!}


<!-- <div class="form-group">
   {!! Form::label('Transaction ID')  !!} 
   {!! Form::text('tid', null, ['class'=>'form-control'])  !!} 

</div> -->

<div class="form-group">
   {!! Form::label('Date')  !!} 
   {!! Form::text('date', null, ['class'=>'form-control'])  !!} 

</div>

<div class="form-group"> 
   {!! Form::label('Total Price') !!} 
   {!! Form::text('totalprice', null, ['class'=>'form-control'])  !!} 

</div>

<div class="form-group">
   {!! Form::label('Customer ID')  !!} 
   {!! Form::text('cid', null, ['class'=>'form-control'])  !!} 

</div>

<div class="form-group">
   {!! Form::label('Paid Details')  !!} 
   {!! Form::text('paid', null, ['class'=>'form-control'])  !!} 

</div>




    {!! Form::submit('Update Transaction', ['class' => 'btn btn-primary']) !!}

    </div>

   {!! Form::close() !!} 

<hr>



@endsection