
@extends('layouts.app')
@section('content')

   <div class="container">
   <h3 align="center">Transactions for</h3>
    <a href="../organizations/{{$org->oid}}"> <u><h3 align="center"> {{ $org->name }}</h3></u> </a>
    </div>
    

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">

 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> Transaction ID </th>
 <th> Date </th>
 <th> Total Price </th>
 <th> Paid Details</th>
 <th> Customer ID</th>
 <th> First Name</th>
 <th> Middle Name</th>
 <th> Last Name</th>

 </thead>
<tbody>
@foreach($data as $row)
<tr>
    <td>{{ $row->tid }}</td>            
    <td>{{ $row->date }}</td>
    <td>{{ $row->totalprice }}</td> 

    @if($row->paid ==1)
         
           <td> <b>Paid<b></td>
         
         @else
           <td><b>Unpaid </b></td>
         
    @endif

    <!-- <td>{{ $row->paid }}</td>             -->
    <td>{{ $row->cid }}</td>
    <td>{{ $row->fname }}</td>            
    <td>{{ $row->mnames }}</td>
    <td>{{ $row->lname }}</td>            
    
          

    <td> <p><a href="{{ route('transactions.show', $row->tid) }}" class="btn btn-info">View transaction</a>
        <a href="{{ route('transactions.edit', $row->tid) }}" class="btn btn-primary">Edit </a>
    </p></td> 
    </tr> 
@endforeach

</tbody>
</table>
</div>
</section>
</div>
</div>




@endsection