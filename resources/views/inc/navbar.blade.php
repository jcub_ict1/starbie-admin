<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
  <div class="container">
      <a class="navbar-brand" href="{{ url('/dashboard/') }}">
        {{ config('app.name', 'test1') }}
    </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
	  @if(Auth::user())
          @if(Auth::user()->hasRole('Admin'))

          <ul class="navbar-nav mr-auto">

              <li class="nav-item">
                  <a class="nav-link" href="/about">About</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/services">Services</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/customers">Customers</a>
              </li>

              <li class="nav-item">
                  <a class="nav-link" href="/admin">Admin</a>
              </li>

              <li class="nav-item">

                  <div class="dropdown nav-link">
                      <a class="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
Branches
</a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="/branch">View Branches </a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="/branch/create">Create a Branch</a>

                      </div>
              </li>
          </ul>
          </div>

          <ul class="navbar-nav ml-auto">

              <li class="nav-item">

                  <div class="dropdown nav-link">
                      <a class="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
Advertisement
</a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          <a class="dropdown-item" href="/advertises">View Advertisement </a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="/advertises/create">Create an Advertisement</a>

                      </div>
              </li>
			 @else 
			  <ul style="list-style:none">
			  <li class="nav-item">

              <div class="dropdown nav-link">
                  <a class="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
Organizations
</a>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="/organizations">View Organizations </a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="/organizations/create">Create an Organization</a>

                  </div>
          </li></ul>
			  @endif
			  @endif
			  
              @guest
              <ul style="list-style: none;">
              <li class="nav-item">
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
              </li></ul>

              @else
              <ul style="list-style:none">
              <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    
                    @if(Auth::user())
                    @if(Auth::user()->hasRole('Super Admin'))

                    <a class="dropdown-item" href="{{ route('superadmin-dashboard') }}">{{ __('Register Admins') }}
   </a>
@endif
@endif
                      <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form></ul>
                      
                  </div>
              </li>
              @endguest
          </ul>
          </div>
      </div>
	  
  </div>
</nav>