@extends('layouts.app')

@section('content')
  
  <div class="container">
   <h3 align="center">Import "Excel File Data" to your Inventory</h3>
   <h3 align ="center"> <b>Easy Peasy </b> </h3>
    <br />
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Download Inventories Data</h5>
            <a href="{{ url('downloadData/xlsx') }}"><button class="btn btn-dark">Download Excel xlsx</button></a>
            <!-- <a href="{{ url('downloadData/xls') }}"><button class="btn btn-success">Download Excel xls</button></a> -->
            <a href="{{ url('downloadData/csv') }}"><button class="btn btn-info">Download CSV</button></a>

            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ url('importData') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                @csrf

                <input type="file" name="import_file" />
                <button class="btn btn-primary">Import File</button>
            </form>

        </div>
    </div>
   
   <br />
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title">Inventory Item</h3>
    </div>
    <div class="panel-body">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <tr>
        <th>Item ID</th>
        <th>Amount Existing</th>
        <th>Amount Shown</th>
        <th>Price Bought</th>
        <th>Price Selling</th>
        <th>Name</th>
        <th>Description</th>
        <th>Gallery</th>
        <th>Expiry Date</th>
        <th>Date Bought</th>
        <th>Category ID</th>  
        <th>Inventory ID</th>               
       </tr>
       @foreach($data as $row)
       <tr>
        <td>{{ $row->itemid }}</td>
        <td>{{ $row->amountexisting }}</td>
        <td>{{ $row->amountshown }}</td>
        <td>{{ $row->pricebought }}</td>
        <td>{{ $row->priceselling }}</td>
        <td>{{ $row->name }}</td>
        <td>{{ $row->description }}</td>
        <td>{{ $row->gallery }}</td>
        <td>{{ $row->expirydate }}</td>
        <td>{{ $row->datebought }}</td>
        <td>{{ $row->catid }}</td>
        <td>{{ $row->iid }}</td>        
       </tr>
       @endforeach
      </table>
     </div>
    </div>
   </div>
  </div>


@endsection