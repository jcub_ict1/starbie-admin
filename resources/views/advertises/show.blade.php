@extends('layouts.app')

@section('title')
    Show advertisements
@endsection
@section('content')
<p><a href ={{route('advertises.index')}} class = "btn btn-primary"> Go back</a></p>
    <h3> Information about advertises</h3>
    <div class="row p-3">


    <form action = "/advertises/{{$advertises->ad_id}}/edit">
        <button class = "btn btn-primary" class="p-5">Edit</button>
    </form>
    <form action = "/advertises/{{$advertises->ad_id}}" method = "POST">
        @method('delete')
        @csrf
        <button value="delete" class="btn btn-danger">Delete</button>
    </form>
    </div>
    <br/>
    <div class="row">
        <div class="col-6">
<p><strong>ID: </strong>{{$advertises->ad_id}}</p>
<p><strong>organization Name: </strong>{{$advertises->oname}}</p>
<p><strong>advertisesment Name</strong>{{$advertises->contactno1}}</p>
<p><strong>advertisesment Message </strong>{{$advertises->contactno2}}</p>
<p><strong>Image </strong>{{$advertises->headquarter}}</p>

</div>
@if($advertises->ad_image)

<div class="row">
<div class="col-6"><img src="{{ asset('storage/' . $advertises->ad_image) }}" alt="Adv logo" height="300" width="400"></div>
</div>
</div>
@endif
@endsection
