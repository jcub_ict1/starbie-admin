@extends('layouts.app')

@section('content')
    <h1>Create Advertisement</h1>
    <form action ="/advertises" method = "post" enctype="multipart/form-data">

        @include('advertises.form')
        <button class ="btn btn-primary">Add Advertisement</button>

    </form>
@endsection
