@extends('layouts.app')

@section('content')
    <h1>Advertises</h1>
    
    <a href="{{route('advertises.create')}}">Create Advertisement</a>
    @if (count($advertises)>0)
        @foreach ($advertises as $advertise)
            <div class = "well">
            <h3><a class="advertiselink" href ="/advertises/{{$advertise->ad_id}}">{{$advertise->oname}}</a></h3>
                <small> written on {{$advertise->created_at}}</small>
            </div>
        @endforeach


      {{-- {{ $posts->links() }} --}}
    @else
        no posts found

    @endif
        


@endsection
