@extends('layouts.app')

@section('content')
    <h1>Edit Advertisement</h1>


    <form action ="/advertisesController@update" method = "post" enctype="multipart/form-data">

        @include('advertises.form')
        <button class ="btn btn-primary">Edit Advertisement</button>
        </form>
    @endsection
