{{ csrf_field() }}

<div class="mb 5">
<div class = "form-group">
        <label for ="oname">Organization Name</label>
        <input type="text" name= "oname" value="{{old('oname') ?? $advertises->oname}} " class ="form-control" placeholder="Organization Name">
        {{-- <input type="text" name= "oname" value="oname " class ="form-control" placeholder="Organization Name"> --}}
         {{$errors->first('oname')}}
    </div>
    <div class = "form-group">
        <label for ="ad_name">advertisesment Name</label>
        <input type="text" name= "ad_name" value="{{old('ad_name') ?? $advertises->ad_name}} " class ="form-control" placeholder="advertisesment Name">
         {{$errors->first('oname')}}
    </div>
    <div class = "form-group">
        <label for ="ad_message">Description</label>
        <input type="text" name= "ad_message" value="{{old('ad_message') ?? $advertises->ad_message}} " class ="form-control" placeholder="Description">
         {{$errors->first('ad_message')}}
    </div>
    <div class = "form-group">
        <label for ="ad_image">advertisesment Image </label>
        <input type="file" name ="ad_image"value ="{{old('ad_image')?? $advertises->ad_image}}" class ="form-control" placeholder="Please Upload advertisesment image">
        <div>{{$errors->first('ad_image')}}</div>
    </div>
</div>
