<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->bigIncrements('oid');
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('themecolor');
            $table->string('contactno1');
            $table->string('contactno2');
            $table->string('headquarter');
            $table->string('about');
            $table->boolean('mustreg')->default(0)->change();
            $table->boolean('canreg')->default(0)->change();
            $table->boolean('mustapprove')->default(0)->change();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization');
    }
}
