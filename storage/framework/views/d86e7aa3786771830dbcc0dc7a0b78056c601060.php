<?php $__env->startSection('title','Create Organizations'); ?>;
<?php $__env->startSection('content'); ?>
<h1> Create organizations</h1>
<form action ="/organizations" method = "post">
    <?php echo $__env->make('organizations.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <button class ="btn btn-primary">Add Organization</button>
   
</form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/organizations/create.blade.php ENDPATH**/ ?>