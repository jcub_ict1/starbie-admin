<?php $__env->startSection('content'); ?>

    <h1><b>Branch Id:</b> <?php echo e($branch->bid); ?></h1>
    <div>
       <h1><b>Branch Description:</b></h1> <h3><?php echo $branch->about; ?></h3>
    </div>
    <hr>
    <!-- <small>Written on <?php echo e($branch ->created_at); ?></small> -->
    <hr>
    <?php echo Form::open(['action'=>['branchcontroller@destroy',$branch->bid],'method'=>'branch','class'=>'pull-right']); ?>

    <a href ="/branch/<?php echo e($branch->bid); ?>/edit" class = "btn btn-primary">Edit</a>
    
        <?php echo e(Form::hidden('_method','Delete')); ?>

        
        <?php echo e(Form::submit('Delete',['class'=>'btn btn-danger'])); ?>

        <a href ="/branch" class = "btn btn-secondary"> Go back to branches</a>
    <?php echo Form::close(); ?>

    
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/branch_pages/branch_show.blade.php ENDPATH**/ ?>