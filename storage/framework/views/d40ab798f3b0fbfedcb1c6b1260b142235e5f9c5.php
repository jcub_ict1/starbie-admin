<?php $__env->startSection('content'); ?>

   <div class="container">
   <h3 align="center">Transactions for</h3>
    <a href="../organizations/<?php echo e($org->oid); ?>"> <u><h3 align="center"> <?php echo e($org->name); ?></h3></u> </a>
    </div>
    

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">

 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> Transaction ID </th>
 <th> Date </th>
 <th> Total Price </th>
 <th> Paid Details</th>
 <th> Customer ID</th>
 <th> First Name</th>
 <th> Middle Name</th>
 <th> Last Name</th>

 </thead>
<tbody>
<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr>
    <td><?php echo e($row->tid); ?></td>            
    <td><?php echo e($row->date); ?></td>
    <td><?php echo e($row->totalprice); ?></td> 

    <?php if($row->paid ==1): ?>
         
           <td> <b>Paid<b></td>
         
         <?php else: ?>
           <td><b>Unpaid </b></td>
         
    <?php endif; ?>

    <!-- <td><?php echo e($row->paid); ?></td>             -->
    <td><?php echo e($row->cid); ?></td>
    <td><?php echo e($row->fname); ?></td>            
    <td><?php echo e($row->mnames); ?></td>
    <td><?php echo e($row->lname); ?></td>            
    
          

    <td> <p><a href="<?php echo e(route('transactions.show', $row->tid)); ?>" class="btn btn-info">View transaction</a>
        <a href="<?php echo e(route('transactions.edit', $row->tid)); ?>" class="btn btn-primary">Edit </a>
    </p></td> 
    </tr> 
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</tbody>
</table>
</div>
</section>
</div>
</div>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/transactions/index.blade.php ENDPATH**/ ?>