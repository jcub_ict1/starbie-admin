<?php $__env->startSection('content'); ?>
<a href ="/posts" class = "btn btn-default"> Go back</a>
    <h1><?php echo e($post->title); ?></h1>
    <div>
        <?php echo $post->body; ?><br/>
        <?php echo $post->logo; ?>

    </div>
    <hr>
    <small>Written on <?php echo e($post ->created_at); ?></small>
    <hr>
    <a href ="/posts/<?php echo e($post->id); ?>/edit" class = "btn btn-default">Edit</a>
    <?php echo Form::open(['action'=>['PostsController@destroy',$post->id],'method'=>'post','class'=>'pull-right']); ?>

        <?php echo e(Form::hidden('_method','Delete')); ?>

        
        <?php echo e(Form::submit('Delete',['class'=>'btn btn-danger'])); ?>

    <?php echo Form::close(); ?>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/posts/show.blade.php ENDPATH**/ ?>