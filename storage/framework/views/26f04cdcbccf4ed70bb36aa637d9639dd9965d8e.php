<?php $__env->startSection('content'); ?>
  
  <div class="container">
   <h3 align="center">Import "Excel File Data" to your Inventory</h3>
   <h3 align ="center"> <b>Easy Peasy </b> </h3>
    <br />
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Download Inventories Data</h5>
            <a href="<?php echo e(url('downloadData/xlsx')); ?>"><button class="btn btn-dark">Download Excel xlsx</button></a>
            <a href="<?php echo e(url('downloadData/xls')); ?>"><button class="btn btn-success">Download Excel xls</button></a>
            <a href="<?php echo e(url('downloadData/csv')); ?>"><button class="btn btn-info">Download CSV</button></a>

            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="<?php echo e(url('importData')); ?>" class="form-horizontal" method="post" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>

                <input type="file" name="import_file" />
                <button class="btn btn-primary">Import File</button>
            </form>

        </div>
    </div>
   
   <br />
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title">Inventory Item</h3>
    </div>
    <div class="panel-body">
     <div class="table-responsive">
      <table class="table table-bordered table-striped">
       <tr>
        <th>Item ID</th>
        <th>Amount Existing</th>
        <th>Amount Shown</th>
        <th>Price Bought</th>
        <th>Price Selling</th>
        <th>Name</th>
        <th>Description</th>
        <th>Gallery</th>
        <th>Expiry Date</th>
        <th>Date Bought</th>
        <th>Category ID</th>  
        <th>Inventory ID</th>               
       </tr>
       <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <tr>
        <td><?php echo e($row->itemid); ?></td>
        <td><?php echo e($row->amountexisting); ?></td>
        <td><?php echo e($row->amountshown); ?></td>
        <td><?php echo e($row->pricebought); ?></td>
        <td><?php echo e($row->priceselling); ?></td>
        <td><?php echo e($row->name); ?></td>
        <td><?php echo e($row->description); ?></td>
        <td><?php echo e($row->gallery); ?></td>
        <td><?php echo e($row->expirydate); ?></td>
        <td><?php echo e($row->datebought); ?></td>
        <td><?php echo e($row->catid); ?></td>
        <td><?php echo e($row->iid); ?></td>        
       </tr>
       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      </table>
     </div>
    </div>
   </div>
  </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/import_excel.blade.php ENDPATH**/ ?>