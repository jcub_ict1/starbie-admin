<?php $__env->startSection('content'); ?>

<div class="row">
<div class= "col-lg-12 col-sm-12">
<section class="panel panel-primary"> 
 <div class="panel-heading" > 
 <hr>
</h3><b>Edit the Customer</b></h3>
 </div>

<div class="panel-body"> 

<?php echo Form::model($customers, [
    'method' => 'PATCH',
    'route' => ['customers.update', $customers->cid]
]); ?>



<div class="form-group">
   <?php echo Form::label('Fist Name'); ?> 
   <?php echo Form::text('fname', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group"> 
   <?php echo Form::label('Middle Name'); ?> 
   <?php echo Form::text('mnames', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Last Name'); ?> 
   <?php echo Form::text('lname', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Date of Birth'); ?> 
   <?php echo Form::text('dob', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Gender'); ?> 
   <?php echo Form::text('gender', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Email Address'); ?> 
   <?php echo Form::text('email', null, ['class'=>'form-control'] ); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Organization ID'); ?> 
   <?php echo Form::text('oid', null, ['class'=>'form-control'] ); ?> 

</div>


<div class="form-group">
   <?php echo Form::label('Approved Status (Enter 1-Approved || 0/Null-Pending'); ?> 
   <?php echo Form::text('approved', null, ['class'=>'form-control'] ); ?> 

</div>


    <?php echo Form::submit('Update Customer', ['class' => 'btn btn-primary']); ?>


    </div>

   <?php echo Form::close(); ?> 

<hr>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/customers/edit.blade.php ENDPATH**/ ?>