<?php $__env->startSection('title'); ?>
    Show advertisements
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<p><a href =<?php echo e(route('advertises.index')); ?> class = "btn btn-primary"> Go back</a></p>
    
        
    
    <h3> Information about advertises</h3>
    <div class="row p-3">


    <form action = "/advertises/<?php echo e($advertises->ad_id); ?>/edit">
        <button class = "btn btn-primary" class="p-5">Edit</button>
    </form>
    <form action = "/advertises/<?php echo e($advertises->ad_id); ?>" method = "POST">
        <?php echo method_field('delete'); ?>
        <?php echo csrf_field(); ?>
        <button value="delete" class="btn btn-danger">Delete</button>
    </form>
    </div>
    <br/>
    <div class="row">
        <div class="col-6">
<p><strong>ID: </strong><?php echo e($advertises->ad_id); ?></p>
<p><strong>organization Name: </strong><?php echo e($advertises->oname); ?></p>
<p><strong>advertisesment Name</strong><?php echo e($advertises->contactno1); ?></p>
<p><strong>advertisesment Message </strong><?php echo e($advertises->contactno2); ?></p>
<p><strong>Image </strong><?php echo e($advertises->headquarter); ?></p>

</div>
<?php if($advertises->ad_image): ?>

<div class="row">
<div class="col-6"><img src="<?php echo e(asset('storage/' . $advertises->ad_image)); ?>" alt="Adv logo" height="300" width="400"></div>
</div>
</div>
<?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/advertises/show.blade.php ENDPATH**/ ?>