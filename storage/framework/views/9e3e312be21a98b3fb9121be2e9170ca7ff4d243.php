<?php $__env->startSection('content'); ?>
    <h1>Advertisement</h1>
    <?php if(count($posts)>0): ?>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $posts): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class = "well">
            <h3><a href ="/posts/<?php echo e($posts->id); ?>"><?php echo e($posts->title); ?></a></h3>
                <small> written on <?php echo e($posts->created_at); ?></small>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      
    <?php else: ?>
        no posts found
    <?php endif; ?>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/posts/index.blade.php ENDPATH**/ ?>