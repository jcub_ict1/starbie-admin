

<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="<?php echo e(url('/dashboard/')); ?>">
            <?php echo e(config('app.name', 'test1')); ?>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="<?php echo e(__('Toggle navigation')); ?>">
            <span class="navbar-toggler-icon"></span>
         </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            
                  <li class="nav-item">
                    <a class="nav-link" href="/about">About</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="/services">Services</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="/customers">Customers</a>
                    </li>
                            <li class="nav-item">
                      
                      <div class="dropdown nav-link">
  <a class ="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
    Administrators
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/admin">Administrators </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="/admin/create">Create</a>
    
  </div>
</div>
                    </li>

                    <li class="nav-item">
                      
                      <div class="dropdown nav-link">
  <a class ="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
    Branches
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/branch">View Branches </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="/branch/create">Create a Branch</a>
    
  </div></li>
  
  <li class="nav-item">
                      
                      <div class="dropdown nav-link">
  <a class ="dropbtn" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
    Organizations
  </a>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="/organizations">View Organizations </a>
    <div class="dropdown-divider"></div>
    <a class="dropdown-item" href="/organizations/create">Create an Organization</a>
    
  </div></li>

</div>
              
                    <ul class ="nav navbar-nav navbar-right">
                        
                      </ul>
            </ul>
            
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                <li class= "nav-item">
                    <a class ="nav-link" href="/posts/create">Create Advertisement</a>
                  </li>
                <?php if(auth()->guard()->guest()): ?>
                
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(route('login')); ?>"><?php echo e(__('Login')); ?></a>
                    </li>
                    <?php if(Route::has('register')): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo e(route('register')); ?>"><?php echo e(__('Register')); ?></a>
                        </li>
                    <?php endif; ?>
                <?php else: ?>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                <?php echo e(__('Logout')); ?>

                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                <?php echo csrf_field(); ?>
                            </form>
                        </div>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final1\ICT_Alpha_Final_Praj\resources\views/inc/navbar.blade.php ENDPATH**/ ?>