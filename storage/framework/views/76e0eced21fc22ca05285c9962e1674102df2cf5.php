<?php $__env->startSection('title','List of '); ?>

<?php $__env->startSection('content'); ?>
<h1> Organization Lists</h1>
<table class="table table-light">
    <tbody>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Contact no</td>
            <td>headquarter</td>  
            <td>theme color</td>  
        </tr>
        <?php $__currentLoopData = $organizations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $orgList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <tr>
        
            <td><?php echo e($orgList->oid); ?></td>
            <td><a href = "/organizations/<?php echo e($orgList->oid); ?>"><?php echo e($orgList->name); ?></td></a>
            <td><?php echo e($orgList->contactno1); ?></td>
            <td><?php echo e($orgList->headquarter); ?></td>
            <td><?php echo e($orgList->themecolor); ?></td>
        </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/organizations/index.blade.php ENDPATH**/ ?>