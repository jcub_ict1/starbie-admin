<?php $__env->startSection('content'); ?>
    <h1>Create Advertisement</h1>
    <form action ="/advertises" method = "post" enctype="multipart/form-data">

        <?php echo $__env->make('advertises.form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <button class ="btn btn-primary">Add Advertisement</button>

    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/advertises/create.blade.php ENDPATH**/ ?>