<?php $__env->startSection('content'); ?>

    <h1>Create Branch</h1>
    
    <?php echo Form::open(['action'=> 'branchcontroller@store','method'=>'POST']); ?>



       <div class = "form-group">
            <label for="name">Organization id:</label>           
            <select name="organization_id" class="form-control">
              <?php $__currentLoopData = $orgs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $org): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                        
                  <option value="<?php echo e(old('org')??$org->oid); ?>"><?php echo e($org->name); ?></option>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </select>
        </div> 
             
    
    <div class = "form-group">
            <?php echo e(Form::label('addressid','Address Id')); ?>

            <?php echo e(Form::text('addressid','',['class'=>'form-control','placeholder'=>'Address Id', 'required'])); ?>

    </div>

    <!-- 
    <div class = "form-group">
        <?php echo e(Form::label('postaladdressid','Postal Address Id')); ?>

        <?php echo e(Form::text('postaladdressid','',['class'=>'form-control','placeholder'=>'Postal Address Id', 'required'])); ?>

    </div> -->

    
    <div class = "form-group">
            <?php echo e(Form::label('pobox','Post Office Box')); ?>

            <?php echo e(Form::text('pobox','',['class'=>'form-control','placeholder'=>'Post Office Box', 'required'])); ?>

    </div>


    
    <div class = "form-group">
            <?php echo e(Form::label('contactno','Contact Number')); ?>

            <?php echo e(Form::text('contactno','',['class'=>'form-control','placeholder'=>'Contact Number', 'required'])); ?>

    </div>

    
    <div class = "form-group">
                <?php echo e(Form::label('about','About Branch')); ?>

                <?php echo e(Form::text('about','',['class'=>'form-control','placeholder'=>'About Branch', 'required'])); ?>

    </div>

    
    <div class = "form-group">
                <?php echo e(Form::label('gallery','Gallery')); ?>

                <?php echo e(Form::text('gallery','',['class'=>'form-control','placeholder'=>'Gallery', 'required'])); ?>

    </div>
    
        <?php echo e(Form::submit('Submit',['class'=>'btn btn-primary'])); ?>

        <?php echo Form::close(); ?>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/branch_pages/branch_create.blade.php ENDPATH**/ ?>