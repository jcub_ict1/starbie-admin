<?php $__env->startSection('content'); ?>

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">
<hr>
 <div class="panel-heading">
 <h3>Transaction Details</h3>
 </div>
 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> Transaction ID</th>
 <th> Date</th>
 <th> Total Price </th>
 <th>Customer ID </th>
 <th> Paid Details </th>
 </thead>
<tbody>
<tr>
    <td><?php echo e($transactions->tid); ?></td>            
    <td><?php echo e($transactions->date); ?></td>
    <td> <?php echo e($transactions->totalprice); ?></td>
    <td> <?php echo e($transactions->cid); ?></td>
    <?php if($transactions->paid ==1): ?>
         
           <td> <b>Paid<b></td>
         
         <?php else: ?>
           <td><b>Unpaid </b></td>
         
    <?php endif; ?>

    </tr>
    </tbody>
    <thead>

</table>
</div>
</section>
</div>
</div>

<div class="row">
<div class="col-md-6"> 
<a href="<?php echo e(route('transactions.index')); ?>" class="btn btn-info">Back to all transactions</a>
<a href="<?php echo e(route('transactions.edit', $transactions->tid)); ?>" class="btn btn-primary">Edit This transaction</a>
</div>


<div class="col-md-6 text-right">
        <?php echo Form::open([
            'method' => 'DELETE',
            'route' => ['transactions.destroy', $transactions->tid]
        ]); ?>

            <?php echo Form::submit('Delete this transaction', ['class' => 'btn btn-danger']); ?>

        <?php echo Form::close(); ?>

    </div>
</div>


<hr>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/transactions/show.blade.php ENDPATH**/ ?>