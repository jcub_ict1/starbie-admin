<?php $__env->startSection('content'); ?>
    <h1>Edit Branches</h1>
    <?php echo Form::open(['action'=> ['branchcontroller@update',$branch->bid],'method'=>'POST']); ?>

    
       
        
        

        <div class = "form-group">
            <label for="name">Organization Id:</label>           
              <select name="oid" class="form-control">
                <?php $__currentLoopData = $orgs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $org): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <option value="<?php echo e($org->oid); ?>"><?php echo e($org->oid); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </select>
            </div>
    
        
        <div class = "form-group">
                <?php echo e(Form::label('addressid','Address Id')); ?>

                <?php echo e(Form::text('addressid',$branch->addressid,['class'=>'form-control','placeholder'=>'Address Id'], 'required')); ?>

            </div>
    
        
        <div class = "form-group">
            <?php echo e(Form::label('postaladdressid','Postal Address Id')); ?>

            <?php echo e(Form::text('postaladdressid',$branch->postaladdressid,['class'=>'form-control','placeholder'=>'Postal Address Id', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                <?php echo e(Form::label('pobox','Post Office Box')); ?>

                <?php echo e(Form::text('pobox',$branch->pobox,['class'=>'form-control','placeholder'=>'Post Office Box', 'required'])); ?>

        </div>
    
    
        
        <div class = "form-group">
                <?php echo e(Form::label('contactno','Contact Number')); ?>

                <?php echo e(Form::text('contactno',$branch->contactno,['class'=>'form-control','placeholder'=>'Contact Number', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                    <?php echo e(Form::label('about','About Branch')); ?>

                    <?php echo e(Form::text('about',$branch->about,['class'=>'form-control','placeholder'=>'About Branch', 'required'])); ?>

        </div>
    
        
        <div class = "form-group">
                    <?php echo e(Form::label('gallery','Gallery')); ?>

                    <?php echo e(Form::text('gallery',$branch->gallery,['class'=>'form-control','placeholder'=>'Gallery', 'required'])); ?>

        </div>
        <?php echo e(Form::hidden('_method','PUT')); ?>

        <?php echo e(Form::submit('Submit',['class'=>'btn btn-primary'])); ?>

    
    <?php echo Form::close(); ?>


    
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/branch_pages/branch_edit.blade.php ENDPATH**/ ?>