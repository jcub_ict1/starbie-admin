<?php $__env->startSection('content'); ?>

<div class="row">
<div class="col-lg-12 col-sm-12">
<section class="panel panel-primary">
<hr>
 <div class="panel-heading">
 <h3>Customer Details</h3>
 </div>
 <div class="panel-body">
 <table class="table table-hover">
 <thead>
 <th> Customer ID</th>
 <th> First Name</th>
 <th> Middle Name </th>
 <th> Last Name </th>
 <th> Organization ID </th>
 <th> Approved Status </th>
 </thead>
<tbody>
<tr>
    <td><?php echo e($customers->cid); ?></td>            
    <td><?php echo e($customers->fname); ?></td>
    <td> <?php echo e($customers->mnames); ?></td>
    <td> <?php echo e($customers->lname); ?></td>
    <td> <?php echo e($customers->oid); ?></td>
    
    <?php if($customers->approved==1): ?>
         
           <td> <b>Approved<b></td>
         
         <?php else: ?>
           <td><b>Pending </b></td>
         
    <?php endif; ?>

    </tr>
    </tbody>
    <thead>
 <th> Date of Birth</th>
 <th> Gender</th>
 <th> Email</th>
 </thead>
<tbody>
<tr>
    <td><?php echo e($customers->dob); ?></td>            
    <td><?php echo e($customers->gender); ?></td>
    <td> <?php echo e($customers->email); ?></td>
    </tr>
    </tbody>
    <thead>
</table>
</div>
</section>
</div>
</div>

<div class="row">
<div class="col-md-6"> 
<a href="<?php echo e(route('customers.index')); ?>" class="btn btn-info">Back to all Customers</a>
<a href="<?php echo e(route('customers.edit', $customers->cid)); ?>" class="btn btn-primary">Edit This Customer</a>
</div>


<div class="col-md-6 text-right">
        <?php echo Form::open([
            'method' => 'DELETE',
            'route' => ['customers.destroy', $customers->cid]
        ]); ?>

            <?php echo Form::submit('Delete this Customer', ['class' => 'btn btn-danger']); ?>

        <?php echo Form::close(); ?>

    </div>
</div>


<hr>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/customers/show.blade.php ENDPATH**/ ?>