<div class = "form-group">
        <label for ="name">Name:</label>
        <input type="text" name= "name" value="<?php echo e(old('name') ?? $organizations->name); ?> " class ="form-control">
         <?php echo e($errors->first('name')); ?>

    </div>
    <div class = "form-group">
        <label for ="contactno1">Mobile Number</label>
        <input type="text" name = "contactno1" value="<?php echo e(old('contactno1')?? $organizations->contactno1); ?>" class ="form-control">
        <?php echo e($errors->first('contactno1')); ?>

    </div>
    <div class = "form-group">
        <label for ="contactno2">Telephone Number</label>
        <input type="text" name = "contactno2" value="<?php echo e(old('contactno2')?? $organizations->contactno2); ?>" class ="form-control">
        <?php echo e($errors->first('contactno2')); ?>

    </div>
    <div class = "form-group">
            <label for ="headquarter">Headquarter</label>
            <input type="text" name ="headquarter" value="<?php echo e(old('headquarter')?? $organizations->headquarter); ?>" class ="form-control">
            <?php echo e($errors->first('headquarter')); ?>

    </div>
    <div class = "form-group">
            <label for ="about">Description</label>
            <textarea name = "about" value="<?php echo e(old('about')?? $organizations->about); ?>" cols="30" rows="10"class ="form-control">
                   <?php echo e($organizations->about); ?>

             </textarea>
            <?php echo e($errors->first('about')); ?>

    </div>
    <?php echo csrf_field(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final\resources\views/organizations/form.blade.php ENDPATH**/ ?>