<?php $__env->startSection('content'); ?>

<div class="row">
<div class= "col-lg-12 col-sm-12">
<section class="panel panel-primary"> 
 <div class="panel-heading" > 
 <hr>
</h3><b>Edit the transaction</b></h3>
 </div>

<div class="panel-body"> 

<?php echo Form::model($transactions, [
    'method' => 'PATCH',
    'route' => ['transactions.update', $transactions->tid]
]); ?>



<!-- <div class="form-group">
   <?php echo Form::label('Transaction ID'); ?> 
   <?php echo Form::text('tid', null, ['class'=>'form-control']); ?> 

</div> -->

<div class="form-group">
   <?php echo Form::label('Date'); ?> 
   <?php echo Form::text('date', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group"> 
   <?php echo Form::label('Total Price'); ?> 
   <?php echo Form::text('totalprice', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Customer ID'); ?> 
   <?php echo Form::text('cid', null, ['class'=>'form-control']); ?> 

</div>

<div class="form-group">
   <?php echo Form::label('Paid Details'); ?> 
   <?php echo Form::text('paid', null, ['class'=>'form-control']); ?> 

</div>




    <?php echo Form::submit('Update Transaction', ['class' => 'btn btn-primary']); ?>


    </div>

   <?php echo Form::close(); ?> 

<hr>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/transactions/edit.blade.php ENDPATH**/ ?>