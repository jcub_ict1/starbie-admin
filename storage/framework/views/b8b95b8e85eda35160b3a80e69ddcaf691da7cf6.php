<?php $__env->startSection('content'); ?>



<div class="d-flex" id="wrapper" >
<h1></h1>
    <!-- Sidebar -->
    <div class=" col-sm-5 col-md-5 bg-light border-right" id="sidebar-wrapper">
      
      <div class="list-group list-group-flush" >
        <a href="/dashboard" class="list-group-item list-group-item-action bg-success"> 
        <img src="<?php echo e(asset('storage/' . $org->logo)); ?>" alt="Organization's logo" height="300" width="400" ><b><h3> <?php echo e($org->name); ?></h3></b>  </a>
         
        <a href="/admin" class="list-group-item list-group-item-action bg-light">Administrator</a>
        <a href="/branch/" class="list-group-item list-group-item-action bg-light">Branches</a>
        <a href="/inventory/" class="list-group-item list-group-item-action bg-light">Inventories</a>
        <a href="/transactions/" class="list-group-item list-group-item-action bg-light">Transactions</a>
        <a href="/stocks/" class="list-group-item list-group-item-action bg-light">Stocks Available</a>
        <a href="/customers/" class="list-group-item list-group-item-action bg-light">Customers</a> 
        <a href="/advertises/" class="list-group-item list-group-item-action bg-light">Advertisement</a>          
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <div class="container-fluid">

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <h1><em>Welcome Admin</em></h1>
                </div>

<div class="table-responsive">
<table class ="table">
<tr>


  <td><div class="btn-group btn-group-lg mr-2" role="group" aria-label="Second group">
   <a href="/admin" class="btn btn-secondary"> Administrators </a>
  </div></td>

<!-- </div></br> -->
<td>
<!-- <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups"> -->
  <div class="btn-group btn-group-lg mr-2" role="group" aria-label="First group">
  <a href="/branch" class="btn btn-secondary"> Branches </a>
   </div></td>
  
   <td>
    <div class="btn-group btn-group-lg mr-2" role="group" aria-label="Second group">
   <a href="/inventory/" class="btn btn-secondary"> Inventories </a> 
    </div></td>
  </tr>
   <tr>
  <td><div class="btn-group btn-group-lg mr-2" role="group" aria-label="Third group">
    <a href="/transactions/" class="btn btn-secondary"> Transactions </a>
  </div></td>
<!-- </div></br> -->
 
 <!-- <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups"> -->
<td>  <div class="btn-group btn-group-lg mr-2" role="group" aria-label="First group">
   <a href="/stocks/" class="btn btn-secondary"> Stock Available </a>
   </div></td></tr>
   <tr><td>
  <div class="btn-group btn-group-lg mr-2" role="group" aria-label="Second group">
    <a href="/customers/" class="btn btn-secondary"> Customers </a>
  </div></td>
  <td><div class="btn-group btn-group-lg mr-2" role="group" aria-label="Third group">
    <a href="/advertises/" class="btn btn-secondary"> Advertisement </a>
  </div></td></tr>
<!-- </div></br> -->
</table>  
</div>
 
 

      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>

  <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/dashboard.blade.php ENDPATH**/ ?>