<?php $__env->startSection('content'); ?>
    <h1>Advertises</h1>
    
    <a href="<?php echo e(route('advertises.create')); ?>">Create Advertisement</a>
    <?php if(count($advertises)>0): ?>
        <?php $__currentLoopData = $advertises; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $advertise): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class = "well">
            <h3><a class="advertiselink" href ="/advertises/<?php echo e($advertise->ad_id); ?>"><?php echo e($advertise->oname); ?></a></h3>
                <small> written on <?php echo e($advertise->created_at); ?></small>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


      
    <?php else: ?>
        no posts found

    <?php endif; ?>
        


<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/advertises/index.blade.php ENDPATH**/ ?>