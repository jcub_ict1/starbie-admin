<?php $__env->startSection('content'); ?>
    <h1>Create Advertisement</h1>
    <?php echo Form::open(['action'=> 'PostsController@store','method'=>'POST','enctype'=>"multipart/form-data"]); ?>

    <div class = "form-group">
        <?php echo e(Form::label('title','Title')); ?>

        <?php echo e(Form::text('title','',['class'=>'form-control','placeholder'=>'Title', 'required'])); ?>

    </div>
    <div class = "form-group">
            <?php echo e(Form::label('body','Body')); ?>

            <?php echo e(Form::textarea('body','',['id'=>'article-ckeditor','class'=>'form-control','placeholder'=>'Body Text','required'])); ?>

    </div>
    <div class="input-group">
                  <!-- <?php echo Form::label('logo', 'Choose Logo'); ?>

                  <?php echo Form::file('image', null); ?> -->
                  <div class ="custom-file">
                     <input type="file" name="logo" class="custom-file-input">
                     <label class ="custom-file-label">Browse file</label>
                  </div>
    </div>
            
        <?php echo e(Form::submit('Submit',['class'=>'btn btn-primary'])); ?>

        <?php echo Form::close(); ?>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\ICT_Alpha_Final\resources\views/posts/create.blade.php ENDPATH**/ ?>