<?php echo e(csrf_field()); ?>


<div class="mb 5">
<div class = "form-group">
        <label for ="name">Name</label>
        <input type="text" name= "name" value="<?php echo e(old('name') ?? $organizations->name); ?> " class ="form-control" placeholder="Organization Name">
         <?php echo e($errors->first('name')); ?>

    </div>
    <div class = "form-group">
        <label for ="contactno1">Mobile Number</label>
        <input type="text" name = "contactno1" value="<?php echo e(old('contactno1')?? $organizations->contactno1); ?>" class ="form-control" placeholder="First Contact Number">
        <?php echo e($errors->first('contactno1')); ?>

    </div>
    <div class = "form-group">
        <label for ="contactno2">Telephone Number</label>
        <input type="text" name = "contactno2" value="<?php echo e(old('contactno2')?? $organizations->contactno2); ?>" class ="form-control" placeholder="Second Contact Number">
        <?php echo e($errors->first('contactno2')); ?>

    </div>
    <div class = "form-group">
            <label for ="headquarter">Headquarter</label>
            <input type="text" name ="headquarter" value="<?php echo e(old('headquarter')?? $organizations->headquarter); ?>" class ="form-control" placeholder="Headquarter">
            <?php echo e($errors->first('headquarter')); ?>

    </div>
    <div class = "form-group">
        <label for ="themecolor">Theme Color</label>
        <input type="text" name ="themecolor" value="<?php echo e(old('themecolor')?? $organizations->themecolor); ?>" class ="form-control" placeholder="Theme Color For Browser">
        <?php echo e($errors->first('themecolor')); ?>

</div>
<div class = "form-group">
    <label for ="logo">Logo</label>
    <input type="file" name ="logo" value="<?php echo e(old('logo')?? $organizations->logo); ?>" class ="form-control" placeholder="Please Upload Organization's Logo">
    <?php echo e($errors->first('logo')); ?>

</div>
    <div class = "form-group d-flex flex-column">
            <label for ="about">Description</label>
            <textarea name = "about" value="<?php echo e(old('about')?? $organizations->about); ?>" cols="30" rows="10"class ="form-control" placeholder="Description about the Organization">
                   <?php echo e($organizations->about); ?>

             </textarea>
            <?php echo e($errors->first('about')); ?>

    </div>
</div>
    

    <?php echo csrf_field(); ?><?php /**PATH C:\xampp\htdocs\ICT_Alpha_Final1\resources\views/organizations/form.blade.php ENDPATH**/ ?>